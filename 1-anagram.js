// input ['cook', 'save', 'taste', 'aves', 'vase', 'state', 'map']
// expectation result :
/**
 [
  [ 'cook' ],
  [ 'save', 'aves', 'vase' ],
  [ 'taste', 'state' ],
  [ 'map' ]
]

 */

function groupAnagrams(input) {
let result = {}; 
for (let word of input) {
     let cleansed = word.split("").sort().join("");
     result[cleansed] ? result[cleansed].push(word) : result[cleansed] = [word] 
   }
return {
   input : input,
   way_grouping : result,
   exprected_result : Object.values(result)
}
}

console.log(groupAnagrams(['cook', 'save', 'taste', 'aves', 'vase', 'state', 'map']))
